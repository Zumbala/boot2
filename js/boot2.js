(function () {
    

    // browsers without indexOf
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(elt /*, from*/) {
            var len = this.length >>> 0,from = Number(arguments[1]) || 0;
            from = (from < 0) ? Math.ceil(from) : Math.floor(from);
            if (from < 0) from += len;
            for (; from < len; from++) if (from in this && this[from] === elt) return from;
            return -1;
        };
    }
    
    // argument or array to array
    function toArray (args,from,til) {
        var a = [];
        from = from || 0; til = til || args.length;
        for (var i = from; i<til; i++) a.push(args[i]);
        a.each = function (func) {
            var args = toArray(arguments);
            args.unshift(this);
            return each.apply(this,args);
        };
        return a;
    }
    
    // call function on each list member
    function each(list,func) {
        var i,j, result;
        if (toType(list) !== "array") list = [list];
        for (i=0,j=list.length;i<j;i=i+1) {
            result = func.call(this,i,list[i]);
            if (result === true) break;
        }
        return result;
    }

    function hasType(obj) {
        if (obj === undefined) return "undefined";
        if (obj === null) return "null";
        var m = obj.constructor;
        if (!m) return "window";
        m = m.toString().match(/(?:function|\[object)\s*([a-z|A-Z|0-9|_|@]*)/);
        return m[1];
    }
    
    // get type of object
    function toType(obj) {
        return hasType(obj).toLowerCase();
    }
    
    // check if object is one of given types
    function isType(obj) {
        var args = toArray(arguments,1);
        return each(args,function(i,item) {
            return (toType(obj) === item) || (item === "*");
        });
    }
    
    // check if object is not of type
    function notType(obj,type) {
        return !isType(obj,type);
    }
    
    // see if object is empty, thus {}
    function isEmpty(obj) {
        for(var i in this) return false; 
        return true;
    }

    // format a string with parameters
    function fmt(stringFormat) {
		var args = toArray(arguments,1), result;
		result = stringFormat.replace(/\{(\d+)\}/g, function(match, number) {
			return isType(args[number],"undefined","null") ? '{' + number + '}' : args[number];
		}); 
		return result ? result : args.join(" ");
	}
    
    // split a text with the use of regexes in given order
    function regExSplit(obj,what) {
        var args = toArray(arguments,2), name, found;
        while (args.length>0) {
            name = args.shift();
            found = args.shift().exec(what);
            obj[name] = "";
            if (found) {
                if (found[1]) obj[name] = found[1];
                what = what.replace(found[0],"");
            }
        }
        return obj;
    }
    
    // splits a file url into its parts
    function fileSplit(file) {
        file = regExSplit({ 
            request : file, 
            toString : function () { return this.request; },
            path : function () { return [this.protocol,this.host,this.directory].join(""); },
            absolute : function (relative) {
                // split the path parts
                var parts = relative.split("/"); parts.pop();
                // transform relative to absolute parts
                var base = this.directory.split("/"); base.pop();
                each(parts,function(i,part) {
                    if (part === "." || part === "") return;
                    if (part === "..") {
                        if (base.length > 0) base.pop(); 
                    } else 
                        base.push(part);
                });
                return base.join("/")+"/";
            },
            relative : function(root) {
                root = fileSplit(root).path();
                var path = this.path(), i = path.indexOf(root);
                if (i!==-1) path = path.substr(i+root.length);
                return path;
            },
            lastFolder : function () {
                var list = this.directory.split("/"), result = "";
                while (result === "" && list.length) result = list.pop();
                return result;
            }
        },file,
            "protocol",/(^https?:\/\/)/,"host",/([\w\d]+(?:\.[\w\d]+){1,2})(?=\/)/,
            "directory",/(\/?(?:(?:[\w\d]+|\.{1,2})\/)+)/,"file",/([^#?]*)/,
            "params",/(\?[^#]+)/,"fragment",/(\#.+)/
        );
        regExSplit(file,file.file,"filename",/((?:.+)(?=\..+)|(?:.+))/,"ext",/(\..+)/);
        return file;
    }
    
    // bind a function to an object
    function bind (func, context) {
        var args = toArray(arguments,2);
        return function () {
            return func.apply(context, args.concat(toArray(arguments)));
        };
    }
    
    // remove proprty from object, IE bug
    function deleteProperty(obj,prop) {
        try {
            delete obj[prop];
        } catch (e) {
            obj[prop] = undefined;
        }
    }
    
    //chain (prop,func) creates a property and if already exists makes inherited available
    function chain(obj,prop,func) {
        var current = obj[prop];
        if (typeof current === "function") {
            if (current === func) return obj;
            var chained = function chain_chained () {
                var result;
                try {
                    this.inherited = function chain_inherited () { return current.apply(this,arguments); };
                    result = func.apply(this,arguments);
                } catch(e) {
                    throw e;
                } finally {
                    deleteProperty(this,"inherited");
                }
                return result;
            };
            obj[prop] = chained;
        } else obj[prop] = func;
        return obj;
    }

    //Object extend (what) extends an object with properties
    function extend (obj) {
        var what,prop,i;
        for (i=1; i<arguments.length; i++) {
            what = arguments[i];
            if (what) for (prop in what) { try { chain(obj,prop,what[prop]); } catch(e) {} }
        }
        return obj;
    }
    
    // obscure function block
    var functionToString = Function.prototype.toString;
    Function.prototype.toString = function () {
        var m = functionToString.call(this).match(/(function)\s*([a-z|A-Z|0-9|_|@]*)\s*(\(.*?\))/),s = "";
        for (var i = 1; i<4; i++) s += (m[i] ? (s ? " " : "")+m[i] : "");
        return s+" { [native code] }";
    };
    
    // function source
    function sourceSplit(func) {
        var source = regExSplit({},functionToString.call(func),"function",/(function) /,"name",/([^\(]*)/,"params",/\((.*)?\)/,"code",/([^\xFFFF]*)/);
        source.params = source.params ? source.params.split(",") : [];
        return source;
    }
    
    // simple Map object, which can store key,value pairs
    function Map(obj) {
        var items = [];
        obj = obj || {};
        obj.autoCreate = false;
        function find(key) {
            var index,found = each(items,function(i,item){
                index = i;
                return (item.key === key);
            });
            return found ? index : -1;
        }
        obj.set = function (key,value) {
            if (find(key) === -1) items.push({key : key, value : value});
            this.length = items.length;
            return value;
        };
        obj.remove = function (key) {
            var i = find(key);
            if ( i === -1) return false;
            items.splice(i,1);
            this.length = items.length;
            return true;
        };
        obj.get = function (key) {
            var i = find(key), result = i !== -1 ? items[i].value : undefined;
            return (this.autoCreate && result === undefined ? this.set(key,null) : result);
        };
        obj.filter = function (func) {
            var result = [];
            each(items,function(i,item) {
                var value = func && func.call(items,i,item) || 0;
                if (value !== undefined) result.push(item);
            });
            return result;
        };
        obj.sort = function (func) {
            items.sort(func || function(a,b) {
                return a.key < b.key;
            });
        };
        return obj;
    }
    
    // message bus controller with priority support
    function Controller(obj) {
        var channels = Map();
        obj = obj || {};
        obj.on = function (name,priority,context,func) {
            var channel = channels.get(name) || channels.set(name,Map()), args = arguments.length;
            if (args === 2) { func = priority; context = this; priority = 100; }
            if (args === 3) { func = context; context = this; }
            var group = channel.get(priority) || channel.set(priority,Map());
            channel.sort();
            if (group.get(func) === undefined) group.set(func,context); 
        };
        obj.emit = function (name) {
            var channel = channels.get(name) || channels.set(name,Map());
            var args = toArray(arguments,1);
            each(channel.filter(),function(i,group) {
                // use filter to get copy of values, else remove skips item
                each(group.value.filter(),function(i,callback) {
                    var result;
                    try {
                        result = callback.key.apply(callback.value,args);
                        // if return value = "remove" then remove function from list
                        if (result === "remove") group.value.remove(callback.key);
                    } catch (e) {}
                });
            });
        };
        obj.msg = function (name /*, arg, arg, ... , array of arguments */) {
            var args = toArray(arguments,0,arguments.length-1);
            this.emit.apply(this,args.concat(arguments[arguments.length-1]));
        };
        obj.cancel = function (name,func) {
            var channel = channels.get(name);
            if (arguments.length === 1 && channel) return channels.remove(name);
            if (arguments.length === 2 && channel) {
                each(channel.filter(),function (i,group) {
                    group.remove(func);
                });
            }
            return false;
        };
        return obj;
    }
    
    // initial vars and values
    var thisModule, FID = 0, loading = [], messages = Controller(), modules = Map(), pending = [], defines = [], defined = {}, ME, App, _i18n;
    
    function Who() {
        var s = toArray(document.getElementsByTagName("script")).pop(), url = resolvePath(document.location.href || document.URL,s.src);
        return { 
            url : url,
            main : s.getAttribute("main") || "",
            global : s.getAttribute("global") || "Application",
            language : s.getAttribute("language") || "EN",
            debug : s.hasAttribute("debug")
        };
    }
    
    // get this modules url
    thisModule = Who();

    // language object for key value substition 
    function Language(anID,language,newdata) {
        var data = newdata || {}, lang = language || "EN", id = anID || "";
        return {
            id : function (anID) {
                id = anID || "";
            },
            set : function (language,newdata) {
                extend(data,newdata); 
                lang = language;
            },
            get : function () { return lang; },
            text : function (key) {
                var txt = data[key];
                if (txt === undefined) messages.emit("warning",id,_i18n.text("language.key",key,lang));
                return fmt.apply(this,[txt || ""].concat(toArray(arguments,1)));
            },
            textN : function (n,key) {
                var arr = data[key], value, txt;
                if (isType(arr,"array")) {
                    if (arr.length === 0) messages.emit("warning",id,_i18n.text("language.ranges",key,lang));
                    arr = toArray(arr); // make a copy
                    while (arr.length) {
                        value = arr.shift();
                        txt = arr.shift();
                        if (isType(value,"string")) {
                            if (value === n || value === "*") break;
                        } else {
                            if (value>=n) break;
                        }
                    }
                } else messages.emit("warning",id,_i18n.text("language.array",key,lang));
                return fmt.apply(this,[txt || ""].concat(toArray(arguments,2)));
            }
        };
    }
    
    // define language support for Boot2, can be overriden by external file
    _i18n = Language(thisModule.url,"EN",{
        "loaded" : "loaded", "missing" : "missing", "error" : "error",
        "resolved" : "resolved", "failed" : "failed",
        "language.key" : "key {0} not defined in language {1}",
        "language.array" : "key {0} is not of type array in language {1}",
        "language.ranges" : "key {0} has no ranges defined in language {1}",
        "Boot2.ready"  : "The main script {0} is ready",
        "debug.notfound" : "The debug module is not found",
        "future.error" : "error in Future : {0}",
        "future.state" : "future {0} is already in state {1}",
        "script.state" : [
            "*", "script {0} is in state {1}"
        ],
        "script.error" : "script error in {0} on line {1} with message '{2}'",
        "args.error" : "wrong number of arguments in {0}",
        "args.type.error" : "argument not of type {0} for '{1}' in {2}",
        "module.ready" : "module {0} ready with state {1}",
        "module.needed" : "module {0} needs none",
        "modules.require" : [
            1, "module {0} not defined",
            -1, "modules {0} not defined"
        ]
    });

    // arguments checker
    function getArguments(args,spec) {
        var result = {}, error = "";
        args = toArray(args);
        spec.path = -1;  
        each(spec.paths,function(i, item) {
            var a = 0;
            result[i] = {names : [], values : {}, error : ""};
            if (args.length <= item.length) each(item,function(n,arg) {
                if (arg.needs) {
                    if (isType(args[a],spec.types[arg.needs])) {
                        result[i].names.push(arg.needs);
                        result[i].values[arg.needs] = args[a];
                        a++;
                    } else {
                        result[i].error = _i18n.text("args.type.error",spec.types[arg.needs],arg.needs,spec.name);
                        return true; // break loop
                    }
                }
                if (arg.optional) {
                    result[i].values[arg.optional] = arg["default"] || "";
                    result[i].names.push(arg.optional);
                    if (isType(args[a],spec.types[arg.optional])) result[i].values[arg.optional] = args[a++]; 
                }
            }); else {
                result[i].error = _i18n.text("args.error",spec.name);
            }
        });
        each(spec.paths,function(i, item) {
            if (error === "") error = result[i].error;
            if (result[i].error === "") spec.path = i;
        });
        if (spec.path !== -1) return result[spec.path].values;
        throw new Error(error);
    }

    // no operation function
    function noop() {}

    // simple promise implementation, returns an object
    
    function Future(obj) {
        var cbs = [], state = "", args, cb, wait = false;
        obj = obj || {};
        obj.id = obj.id || ++FID;
        function exec(s,a) {
            if (wait) return;
            if (state !== s && state !== "") messages.emit("warning",_i18n.text("future.state",obj.id,state));
            state = state || s; args = args || a; obj.state = state; wait = true;
            while ((cb = cbs.shift())) {
               try {
                    cb.apply({},args);
                } catch(e) {
                    messages.emit("error",_i18n.text("future.error",e.toString()));
                }
            }
            wait = false;
        }
        obj.fail = function () { exec("failed",toArray(arguments)); return this; };
        obj.resolve = function () { exec("resolved",toArray(arguments)); return this; };
        obj.status = function () { return state ? state : "unresolved"; };
        obj.resolved = function (resolved) {
            return this.then(resolved,undefined);
        };
        obj.failed = function (failed) {
            return this.then(undefined,failed);
        };
        obj.always = function (resolvedORfailed) {
            return this.then(resolvedORfailed,resolvedORfailed);
        };
        obj.then = function (resolved,failed) {
            cbs.push(function() {
                var arg = [state].concat(toArray(arguments));
                if (state === "failed" && failed) return failed.apply(obj,arg); 
                if (state === "resolved" && resolved) return resolved.apply(obj,arg); 
            });
            if (state !== "") exec(state,args);
            return this;
        };
        obj.constructor = Future;
        return obj;
    }
    
    // watch a list of promises for completion, when all are ready call resolver
    function Needed(resolver, amount) {
        var ready = false, n = 0, c = 0, stats = [], obj = {
            required : function () { return n },
            future : function () {
                var nr = n++;
                return function(state) {
                    c++;
                    stats[nr] = state; 
                    isReady(); 
                };
            }
        };
        function isReady() {
            if (c !== amount || ready) return;
            ready = true;
            if (each(stats,function (i,item) { return item === "failed"; }) === true) resolver.fail(); else resolver.resolve();
        }
        return obj;
    }

    // do not remove below remark as it is executed in IE to determine the IE version needed to correctly load the scripts
    var IEversion = 100;
    /*@cc_on
        IEversion = document.documentMode || 0;
    @*/
    
    // load a script returning a promise
    function script (url) {
        var cfg = { 
            tag : document.createElement("script"), 
            state : "loading",
            states : {
                "loadingcomplete" : "loaded", "loadingloaded" : "loaded", 
                "loadingmissing" : "missing", "loadingerror" : "error"
            },
            ready : false, 
            promise : Future(),
            toState : function (to) {
                this.state = this.states[this.state+to] || this.state;
                return this.state;
            },
            url : url,
            setState : function (what, msg) {
                if (this.ready) return;
                this.toState(what);
                this.tag.setAttribute("state",this.state);
                if (/loaded|error|missing/.test(this.state) === true) {
                    this.ready = true;
                    this.tag = this.tag.onerror = this.tag.onload = null;
                    deleteProperty(loading,this.url);
                    messages.emit("debug","script",_i18n.textN(what,"script.state",this.url,_i18n.text(this.state)));
                    if (this.state === "loaded") this.promise.resolve(this.url); else this.promise.fail(this.url,this.state,msg || "error");
                }
            },
            onload : function (e,msg,line) {
                msg = msg || window.error; line = line || window.errorline;
                var missing = /script/i.test(msg) === true && line === 0;
                this.setState(missing ? "missing" : msg ? "error" : "loaded",msg); 
            }
        };
        cfg.tag.setAttribute("type","text/javascript");
        cfg.tag.charset = "utf-8";
        cfg.tag.async = false;
        cfg.tag.onerror = bind(function () { this.setState("missing","onerror"); },cfg);
        // IE
        if (cfg.tag.onreadystatechange === null && IEversion < 10) {
            pending.push(cfg);
            loading[url] = cfg.tag.onreadystatechange = function (e,msg,line) { 
                var script;
                // still loading ???
                while (pending[0] && pending[0].tag.readyState === "loaded") {
                    // execute in order
                    script = pending.shift();
                    // no more testing
                    script.tag.onreadystatechange = null;
                    loading[script.url] = bind(script.onload,script);
                    // start executing script code
                    document.scripts[0].parentNode.insertBefore(script.tag, document.scripts[0]);
                    // process defines by resolver
                    script.onload(e,msg,line);
                }
            };
            // start loading
            cfg.tag.setAttribute("src",url);            
        } else {
        // OTHER
            cfg.tag.setAttribute("src",url);
            loading[url] = cfg.tag.onload = bind(cfg.onload,cfg);
            document.getElementsByTagName("head")[0].appendChild(cfg.tag);
        }
        
        return cfg.promise;
    }
    
    // trap all errors from loading an external script
    window.onerror = function (msg, url, line) { 
        url = url || (document.currentScript && document.currentScript.src); // MOZ
        messages.emit("error",_i18n.text("script.error",url,line,msg));
        // NO URL, IS CROSS ORIGIN, so presume missing
        window.error = [msg,"on line",line,"of",url].join(" ");
        window.errorline = line;
        // if running in cache call loading function with exception
        loading[url] && loading[url](null,msg,line);
        deleteProperty(window,"error");
        deleteProperty(window,"errorline");
        return true;
    };
    
    function isLibType(url) {
        url = fileSplit(url);
        if (url.directory && url.file === "") return "module";
        if (url.ext === ".js") return "file";
        return "alias";
    }
    
    // gets a full url for a given module
    function resolvePath(root,url) {
        root = fileSplit(root); url = fileSplit(url); 
        if (url.host === "") {
            // normalize against current root
            url.directory = root.absolute(url.directory);
            url.host = root.host;
            url.protocol = root.protocol;
        }
        return url.path()+url.file;
    }
    
    // define a module, all arguments are optional
    // definition function or object/value
    function defineModule(name, definition, future) {
        var named = modules.get(name) || modules.set(name,Future());
        definition = (typeof definition === "function" ? definition.call(this) : definition);
        var module = defined[name] || definition || {};
        if (module !== definition) extend(module,definition);
        if (name) defined[name] = module;
        if (named && future && named.status() === "unresolved") future.then(named.resolve,named.fail);
        return module;
    }
    
    // define a callback with context to this module
    var cblevel = 0;
    function callback(module,func,defs) {
        return function () {
            var args = [], result;
            each(defs,function(i,name) {
                args.push(notType(name,"string") ? name : defined[name]);
            });
            try {
                ++cblevel;
                window.define = module.define;
                window.require = module.require;
                result = func.apply({ module : module, state : module.state },args);
            } catch(e) {
                throw e;
            } finally {
                --cblevel;
                window.define = cblevel === 0 ? _define : module.define;
                window.require = cblevel === 0 ? _require : module.require;
            }
            return result;
        };
    }

    // loads a module relative to a root
    function Module(url,root) {
        // see if this is javascript file or alias
        url = url.toLowerCase();
        var deps = [], moduleType = isLibType(url), module, needs, waitfor;
        
        if (moduleType !== "alias") {
            url += moduleType === "module" ? "js.js" : "";
            root = root || url;
            url = resolvePath(root,url);
            root = url;
        }
        
        messages.emit("debug","module","root",root,"url",url);
        
        // see if module is already defined, if not define it
        module = modules.get(url) || modules.set(url,Future());
        waitfor = Future({id : url});
        waitfor.always(function (state) {
            messages.emit("info",_i18n.text("module.ready",waitfor.id,_i18n.text(state)));
        });
        
        // language support for this module, always resolve even if language file is not present
        function i18nLoad(url,dlang,resolver) {
            url = fileSplit(url);
            var clang, name = moduleType === "module" ? url.relative(root): url.filename+"/";
            
            function i18nLoaded(lang) {
                module.i18n.set(clang,lang);
                resolver.resolve();
            }
            
            // get defined languages for unit
            module.require(name+"languages.js",function(langs) {
                clang = require("lang").get();
                clang = langs[clang] ? clang : dlang;
                require(name+clang+".js",i18nLoaded,function () {
                    if (dlang === clang) return resolver.resolve();
                    clang = dlang;
                    require(name+clang+".js",i18nLoaded,resolver.resolve);
                });
            },resolver.resolve);
        }
        
        // when module is loaded, see if language support is needed
        module.then(function(state) {
            if (defined[url] && defined[url].i18n) i18nLoad(url,defined[url].i18n,waitfor); else waitfor.resolve();
        },waitfor.fail);
        
        // if alias set as loaded
        if (moduleType === "alias") module.script = url;

        // get all defines and requires waiting in Q
        function getQ(state,url) {
            deps.splice.apply(deps,[deps.length,0].concat(defines));
            defines.length = 0;
            needs = Needed(module,deps.length);
            each(deps, function (i,def) {
                try {
                    var mod = module[def[0]].apply(module,def[1]);
                    if (isType(mod,"future")) mod.always(needs.future());
                } catch(e) {
                    needs.future().call({},"failed");
                    messages.emit("error",_i18n.text("script.error",url,"?",e.message));
                }
            });
        }

        if (!module.i18n) {
            module.i18n = url === thisModule.url ? _i18n : Language(url);
            module.i18n.path = fileSplit(url).path();
            messages.on("language.change",1,module.languagechange = function () {
                if (defined[url] && defined[url].i18n) i18nLoad(url,defined[url].i18n,Future());
            });
        }
        
        // internal define
        if (!module.define) module.define = function () {
            var params = {
                name : "define",
                types : { name : "string", what : "*"},
                paths : [
                    [{optional : "name"},{needs : "what"}]
                ]
            }, args = getArguments(arguments,params), value, future = Future(), isModule, todefine;
            isModule = args.name === "";
            args.name = args.name || url;
            if (isType(args.what,"function")) args.what = callback(module,args.what,[module.i18n]);
            try {
                value = defineModule(args.name,args.what,future);
                // when named add to module it belongs to under name of definition
                if (!isModule && value!==undefined && args.name !== url) { 
                    todefine = {};
                    todefine[args.name] = value;
                    defineModule(url,todefine);
                }
            } catch(e) {
                throw e;
            } finally {
                if (value === undefined) future.fail(); else future.resolve(value);
            }
            return future;
        };

        // internal require
        if (!module.require) module.require = function () {
            var params = {
                name : "require",
                types : { url : "string", name : "string", combine : "boolean", ondefine : "function", onfail : "function", needs : "array"},
                paths : [
                    [{needs : "url"}, {needs : "ondefine"}, {optional : "onfail"}],
                    [{needs : "needs"}, {optional : "combine"}, {needs : "ondefine"}, {optional : "onfail"}],
                    [{needs : "name"}], [{needs : "needs"}]
                ]
            },
            args = getArguments(arguments,params), required, result;
            switch (params.path) {
                // url, ondefine, onfail
                case 0:
                    result = Module(args.url,root);
                    args.url = result.script;
                    args.ondefine = callback(module,args.ondefine,[args.url]);
                    args.onfail = args.onfail ? callback(module,args.onfail) : undefined;
                    result.then(args.ondefine,args.onfail);
                    break;
                // needs, combine, ondefine, onfail
                case 1:
                    result = Future(); required = Needed(result,args.needs.length);
                    args.onfail = args.onfail ? callback(module,args.onfail) : undefined;
                    args.urls = [];
                    // load dependencies and register to this module
                    each(args.needs,function (i, url) {
                        args.urls.push(module.require(url,noop).always(required.future()).script);
                    });
                    result.then(function(state) {
                        // all dependencies loaded, combine modules if specified
                        var requires = args.urls;
                        if (args.combine) requires = module.require(args.needs);
                        callback(module,args.ondefine,requires)();
                    },args.onfail);
                    break;
                // name
                case 2:
                    var mtype = isLibType(args.name);
                    if (mtype !== "alias") args.name = resolvePath(root,args.name + (mtype === "module" ? "js.js" : "")).toString();
                    if (defined[args.name] === undefined) throw new Error(_i18n.textN(1,"modules.require",args.name));
                    result=defined[args.name];
                    break;
                // needs
                case 3:
                    var missing = [];
                    result = {};
                    each(args.needs,function (i,url) {
                        var mtype = isLibType(url), name = fileSplit(url).filename;
                        if (mtype !== "alias") {
                            url = resolvePath(root,url + (mtype === "module" ? "js.js" : "")).toString();
                            if (mtype === "module") name = fileSplit(url).lastFolder();
                        }
                        if (defined[url] === undefined) missing.push(url); else result[name] = defined[url];
                    });
                    if (missing.length) throw new Error(_i18n.textN(missing.length,"modules.require",missing.join(","))); 
            }
            return result;
        };

        // is module called by this module
        if (url === thisModule.url) {
            module.script = url;
            Future().then(getQ,module.fail).resolve();
        }
        // is script already loading
        if (module.script === undefined) {
            module.script = url;
            script(url).then(getQ,module.fail).resolved(function (state) {
                // external file
                if (needs.required() === 0) {
                    messages.emit("debug","module",_i18n.text("module.needed",url));
                    module.resolve();
                }
            });
        }
        return extend(waitfor, {
            require : module.require,
            define : module.define,
            i18n : module.i18n,
            script : module.script
        });
    }
    
    // global define, just pushes it on Q
    function _define(name, what) {
        defines.push(["define",toArray(arguments)]);
    }
    
    // global require, just pushes it on Q
    function _require(url,ondefine,onerror) {
        defines.push(["require",toArray(arguments)]);
    }
    
    // publish global functions
    extend(window, {
        define : _define, require : _require
    });
    
    // boot2
    define("util",{
        toArray : toArray, toType : toType, isType : isType, hasType : hasType,
        notType : notType, isEmpty : isEmpty, regExSplit : regExSplit, fmt : fmt,
        fileSplit : fileSplit, sourceSplit : sourceSplit, getArguments : getArguments,
        each : each, bind : bind, extend : extend,
        deleteProperty : deleteProperty
    });
    define("classes", {
        Future : Future, Needed : Needed, Map : Map, Controller : Controller, Language : Language
    });
    define("dom", function (i18n) {
        var whenready = Future();
        function  isReady() {
            if (/loaded|complete|completed/.test(document.readyState) === false) return setTimeout(isReady,0);
            whenready.resolve();
        }
        isReady();
        return {
            ready : function (func) {
                whenready.always(callback(ME,func,[i18n]));
                return this;
            },
            element : function (id) {
                return document.getElementById(id);
            },
            tags : function (what) {
                return document.getElementsByTagName(what);
            }
        };
    });
    define("messages", messages);
    define("lang", function () {
        var lang = thisModule.language;
        return {
            set : function (language) {
                lang = language;
                messages.emit("language.change",lang);
                return lang;
            },
            get : function() {
                return lang;
            }
        };
    });
    define({
        version : "1.4", date : "2013/09/22", module : "boot2", i18n : "EN"
    });

    
    // initialize boot2
    // load this module definitions
    ME = Module(thisModule.url);
    
    // publish starting point
    App = {
        require : ME.require, define : ME.define, main : thisModule.main
    };
    
    window[thisModule.global] = App;
    
    window["console"] = window["console"] || { log : noop };
    
    ME.define(function(i18n) {
        var m = require(["messages","dom"]);
        if (thisModule.debug) require("debug/", function (debug) {
            App.modules = defined;
            debug.on(); 
        },function () { 
            m.messages.emit("info",i18n.text("debug.notfound")); 
        });
        m.dom.ready(function () {
            if (App.main) require(App.main,function() {
                m.messages.emit("info",i18n.text("Boot2.ready",App.main));
            });
        });
    });
    
})();
